/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package UTP_ZAOCZNE_6B_1;


public class Main {

  public static void main(String[] args) {
    Calc c = new Calc();
    String wynik = c.doCalc(args[0]);
    System.out.println(wynik);
  }

}
