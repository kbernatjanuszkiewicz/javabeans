/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package UTP_ZAOCZNE_6B_2;


import java.beans.*;
import java.io.Serializable;

public class Purchase implements Serializable {

    private String prod;
    private String data;
    private Double price;

    public Purchase(String prod, String data, Double price) {
        this.prod = prod;
        this.data = data;
        this.price = price;
    }

    private PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChange.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChange.removePropertyChangeListener(listener);
    }


    private VetoableChangeSupport vetos = new VetoableChangeSupport(this);
    public synchronized void addVetoableChangeListener(VetoableChangeListener listener) {
        vetos.addVetoableChangeListener(listener);
    }

    public synchronized void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetos.removeVetoableChangeListener(listener);
    }



    synchronized void setProd(String newProd) {
        String oldProd = prod;
        data = newProd;

        propertyChange.firePropertyChange("data", new String(oldProd), new String(newProd));
    }

    public String getProd() {
        return prod;
    }

    public synchronized void setData(String newData) {
        String oldData = data;
        data = newData;

        propertyChange.firePropertyChange("data", new String(oldData), new String(newData));
    }

    public String getData() {
        return data;
    }

    synchronized void setPrice(Double newPrice) throws PropertyVetoException {
        Double oldPrice = price;
        vetos.fireVetoableChange("price", new Double(oldPrice), new Double(newPrice));
        price = newPrice;
        propertyChange.firePropertyChange("price", new Double(oldPrice), new Double(newPrice));
    }


    public String toString() {
        return "Purchase " + "[prod=" + prod + ", data=" + data + ", " + "price=" + price + "]";
    }

}
