package UTP_ZAOCZNE_6B_2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class BoundListener implements PropertyChangeListener {
    public void propertyChange(PropertyChangeEvent e)  {
        if(e.getPropertyName().equals("data")) {
            String oldVal = (String) e.getOldValue(),
                    newVal = (String) e.getNewValue();
            System.out.println("Change value of: " + e.getPropertyName() + " from: "+ oldVal + " to: " + newVal);
        }

        if(e.getPropertyName().equals("price")) {
            Double oldVal = (Double) e.getOldValue(),
                    newVal = (Double) e.getNewValue();
            System.out.println("Change value of: " + e.getPropertyName() + " from: "+ oldVal + " to: " + newVal);
        }
    }
}
