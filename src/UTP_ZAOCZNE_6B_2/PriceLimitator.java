package UTP_ZAOCZNE_6B_2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public class PriceLimitator implements VetoableChangeListener {
    private Double min;

    PriceLimitator(Double minLim) {
        min = minLim;
    }

    public void vetoableChange(PropertyChangeEvent e)
            throws PropertyVetoException {
        Double newVal = (Double) e.getNewValue();
        Double val = newVal.doubleValue();
        // Sprawdzamy, czy zmiana  licznika jest dopuszczalna,
        // jeśli nie – sygnalizujemy wyjatek  PropertyVetoException
        if (val < min) {
            throw new PropertyVetoException("Price change to: " + val + " not allowed", e);
        }
    }


}
